
package hot_number;

import java.util.Random;
import java.util.Date;
import java.util.EventObject;
import java.util.Random;
import java.util.Scanner;


public class Hot_number {

    public static void main(String[] args) {
        
        int num;
        
        Scanner ler = new Scanner(System.in);
        
        System.out.println("Introduza um nº entre 0 a 100: ");
        num = ler.nextInt();
        
        Random rand = new Random();
        
        int diferenca=0;
        
        int n = rand.nextInt(100) + 1;
        
        diferenca = num - n;
        
        diferenca = Math.abs(diferenca);
        
        if (diferenca>25){
            
            System.out.println("Frio");
            
        }else if(diferenca<5){
            
            System.out.println("Quente");
            
        }else if(diferenca>=5 && diferenca<=25){
            
            System.out.println("Morno");
            
        }else if(diferenca==n){
            
            System.out.println("Acertaste");
            
        }else{
            
            System.out.println("Erraste");
            
        }
        
        System.out.println("Random Number:" +n);
        System.out.println("Diferença: "+diferenca);
        
    }
    
}
